window.addEventListener('DOMContentLoaded', async () => {
  function createCard(name, description, pictureUrl, dateStarts, dateEnds, location) {
      return `
        <div class="card">
          <div class="shadow p-3 mb-5 bg-body-tertiary rounded">
          <img src="${pictureUrl}" class="card-img-top">
          <div class="card-body">
            <h5 class="card-title">${name}</h5>
            <h6 class="card-subtitle mb-2 text-muted">${location}</h6>
            <p class="card-text">${description}</p>
          </div>
          <div class="card-footer">
          ${dateStarts} - ${dateEnds}
          </div>
        </div>
        </div>
      `
    }
const url = 'http://localhost:8000/api/conferences/';
try {
const response = await fetch(url);
if (!response.ok) {
  console.error('There was an error in the response')
} else {
      const data = await response.json();
      for (let conference of data.conferences) {
          const detailUrl = `http://localhost:8000${conference.href}`;
          const detailResponse = await fetch(detailUrl);
          if (detailResponse.ok) {
              const details = await detailResponse.json();
              const name = details.conference.name;
              const description = details.conference.description;
              const pictureUrl = details.conference.location.picture_url;
              const starts = new Date(details.conference.starts);
              const dateStarts = starts.toLocaleDateString();
              const ends = new Date(details.conference.ends)
              const dateEnds = ends.toLocaleDateString();
              const location = details.conference.location.name;
              const html = createCard(name, description, pictureUrl,dateStarts,dateEnds, location);
              const column = document.querySelector('.row');
              column.innerHTML += html;
      }
  }
}
} catch (e) {
  console.log(e)
}
});
